/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comedor.altamar;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.apache.commons.io.IOUtils;

/**
 *
 * @author expploitt
 */
public class JVentana extends JFrame {

    private final JMenuBar menuBarra;
    private final JMenu editar;
    private final JMenu ver;

    private final JMenu borrar;
    private final JMenu ayuda;
    private final JMenuItem licencia;
    private final JMenuItem instrucciones;
    //private final JMenuItem contacto;
    private final JMenuItem nuevaPersona;
    private final JMenuItem eliminarPersona;
    private final JMenuItem eliminarComida;
    private final JMenuItem guardar;
    private final JMenuItem actualizar;

    private final JMenuItem personas;
    private final JMenu comidass;
    private final JMenuItem comidasAct;
    private final JMenuItem comidasFut;
    private final JMenuItem borrar1;
    private final JMenuItem borrar2;
    private final JMenu imprimir1;
    private final JMenuItem imprimirPersonas;
    private final JMenuItem imprimirComidas;
    private JTable tabla;
    private JScrollPane scrollPane;

    private final String editarBoton1 = "Nuevo residente      ";
    private final String editarBoton2 = "Eliminar residente   ";
    private final String editarBoton5 = "Eliminar comida     ";
    private final String editarBoton3 = "Guardar cambios     ";
    private final String editarboton4 = "Borrar       ";
    private final String editarboton6 = "Cambio de semana   ";

    private final String verPersonas = "Personas  ";
    private final String verComidas = "Comidas  ";
    private final String menu1 = "Editar";
    private final String menu2 = "Ver";
    private final String borrar11 = "Personas   ";
    private final String borrar22 = "Comidas    ";
    private final String imprimir = "Imprimir";
    private final String imprimirPer = "Personas";
    private final String imprimirCom = "Comidas";
    private final String semAct = "Semana actual  ";
    private final String semFut = "Semana siguiente";
    private final String help = "Ayuda";
    private final String error10 = "  No existen personas en el sistema.\n Por favor, introduzca una nueva.";
    private final String licen = "Licencia  ";
    private final String instruc = "Instrucciones  ";
    //private final String contac = "Contacto  ";

    public JVentana() {

        super("Comedor Altamar");
        setSize(700, 500);
        setIconImage(new ImageIcon(getClass().getResource("/images/iconPrincipal.gif")).getImage());

        menuBarra = new JMenuBar();
        editar = new JMenu(menu1);
        ver = new JMenu(menu2);
        imprimir1 = new JMenu(imprimir);
        ayuda = new JMenu(help);
        nuevaPersona = new JMenuItem(editarBoton1);
        scrollPane = new JScrollPane();

        nuevaPersona.addActionListener((ActionEvent ae) -> {
            System.out.println("Añadiendo nuevo residente...");
            JVentana.this.añadirPersona();
        });

        eliminarPersona = new JMenuItem(editarBoton2);
        eliminarPersona.addActionListener((ae) -> {
            System.out.println("Eliminando residente...");
            this.eliminarPersona();

        });

        eliminarComida = new JMenuItem(editarBoton5);
        eliminarComida.addActionListener((e) -> {
            System.out.println("Eliminando comida...");
            this.eliminarComida();
        });
        guardar = new JMenuItem(editarBoton3);
        guardar.addActionListener((e) -> {
            System.out.println("Guardando...");
            new Ficheros().escribirPersonas();
            new Ficheros().escribirComidas();
            JOptionPane.showMessageDialog(rootPane, "Guardado.");

        });

        personas = new JMenuItem(verPersonas);
        personas.addActionListener((ActionEvent e) -> {
            System.out.println("Visualizando personas del sistema");
            remove(scrollPane);
            tabla = new JTable(tablaPersonas());
            tabla.setEnabled(false);
            tabla.setRowHeight(70);
            TableRowSorter<TableModel> sorter
                    = new TableRowSorter<>(tabla.getModel());
            List<RowSorter.SortKey> sortKeys
                    = new ArrayList<>();
            sortKeys.add(new RowSorter.SortKey(1, SortOrder.ASCENDING));
            sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
            sorter.setSortKeys(sortKeys);
            tabla.setRowSorter(sorter);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
            tabla.getColumn("Nombre").setCellRenderer(rightRenderer);
            tabla.getColumn("Apellidos").setCellRenderer(rightRenderer);
            tabla.getColumn("DNI").setCellRenderer(rightRenderer);

            scrollPane = new JScrollPane(tabla);

            this.add(scrollPane, BorderLayout.CENTER);
            this.pack();

        });

        comidass = new JMenu(verComidas);

        comidasAct = new JMenuItem(semAct);
        comidasFut = new JMenuItem(semFut);

        comidasAct.addActionListener((e) -> {
            System.out.println("Visualizando comidas de esta semana del sistema");
            remove(scrollPane);
            tabla = new JTable(this.tablaComidasActuales());
            //  tabla.setEnabled(false);
            tabla.setRowHeight(70);

            TableRowSorter<TableModel> sorter
                    = new TableRowSorter<>(tabla.getModel());
            List<RowSorter.SortKey> sortKeys
                    = new ArrayList<>();
            sortKeys.add(new RowSorter.SortKey(1, SortOrder.ASCENDING));
            sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
            sorter.setSortKeys(sortKeys);
            tabla.setRowSorter(sorter);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
            tabla.getColumn("LUNES").setCellRenderer(rightRenderer);
            tabla.getColumn("MARTES").setCellRenderer(rightRenderer);
            tabla.getColumn("MIERCOLES").setCellRenderer(rightRenderer);
            tabla.getColumn("JUEVES").setCellRenderer(rightRenderer);
            tabla.getColumn("VIERNES").setCellRenderer(rightRenderer);
            tabla.getColumn("NOMBRE").setCellRenderer(rightRenderer);

            scrollPane = new JScrollPane(tabla);
            this.add(scrollPane, BorderLayout.CENTER);
            this.pack();
        });

        comidasFut.addActionListener((e) -> {

            System.out.println("Visualizando comidas de esta semana del sistema");
            remove(scrollPane);
            tabla = new JTable(this.tablaComidasFuturas());

            tabla.setRowHeight(70);

            TableRowSorter<TableModel> sorter
                    = new TableRowSorter<>(tabla.getModel());
            List<RowSorter.SortKey> sortKeys
                    = new ArrayList<>();
            sortKeys.add(new RowSorter.SortKey(1, SortOrder.ASCENDING));
            sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
            sorter.setSortKeys(sortKeys);
            tabla.setRowSorter(sorter);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
            tabla.getColumn("LUNES").setCellRenderer(rightRenderer);
            tabla.getColumn("MARTES").setCellRenderer(rightRenderer);
            tabla.getColumn("MIERCOLES").setCellRenderer(rightRenderer);
            tabla.getColumn("JUEVES").setCellRenderer(rightRenderer);
            tabla.getColumn("VIERNES").setCellRenderer(rightRenderer);
            tabla.getColumn("NOMBRE").setCellRenderer(rightRenderer);

            scrollPane = new JScrollPane(tabla);
            this.add(scrollPane, BorderLayout.CENTER);
            this.pack();
        });

        borrar = new JMenu(editarboton4);
        borrar1 = new JMenuItem(borrar11);
        borrar2 = new JMenuItem(borrar22);

        borrar1.addActionListener((e) -> {
            System.out.println("Borrando personas...");
            new Ficheros().borrarPersonas();
            ComedorAltamar.alberguistas.clear();

        });
        borrar2.addActionListener((e) -> {
            System.out.println("Borrando comidas...");
            new Ficheros().borrarComidas();
            ComedorAltamar.comidas.clear();
        });

        imprimirPersonas = new JMenuItem(imprimirPer);
        imprimirPersonas.addActionListener((e) -> {
            tabla = new JTable(tablaPersonas());
            imprimir(tabla);
        });

        imprimirComidas = new JMenuItem(imprimirCom);
        imprimirComidas.addActionListener((e) -> {
            tabla = new JTable(tablaComidasActuales());
            imprimir(tabla);
        });

        licencia = new JMenuItem(licen);
        licencia.addActionListener((ActionEvent e) -> {

            try {
                remove(scrollPane);

                String line;

                InputStream out = this.getClass().getResourceAsStream("/resource/gpl.txt");

                line = IOUtils.toString(out, "utf-8");
                JTextArea label = new JTextArea(line);
                label.setEditable(false);
                scrollPane = new JScrollPane(label);
                scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
                add(scrollPane, BorderLayout.CENTER);
                pack();
            } catch (IOException ex) {
                Logger.getLogger(JVentana.class.getName()).log(Level.SEVERE, null, ex);
            }

        });

        instrucciones = new JMenuItem(instruc);
        instrucciones.addActionListener((e) -> {
            try {
                remove(scrollPane);

                String line;

                InputStream out = this.getClass().getResourceAsStream("/resource/instrucciones.txt");

                line = IOUtils.toString(out, "utf-8");
                JTextArea label = new JTextArea(line);
                label.setEditable(false);
                scrollPane = new JScrollPane(label);
                scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
                add(scrollPane, BorderLayout.CENTER);
                pack();
            } catch (IOException ex) {
                Logger.getLogger(JVentana.class.getName()).log(Level.SEVERE, null, ex);
            }

        });

        actualizar = new JMenuItem(editarboton6);
        actualizar.addActionListener((e) -> {
            /*SI ES JUEVES Y SON LAS 20:00 */
            Calendar calendar = Calendar.getInstance();
            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY ) {
                try {
                    /*PASAMOS LOS DATOS DE LA PRÓXIMA SEMANA A LOS DE ESTA*/
                    new Ficheros().copiarArchivo(Ficheros.FILE_COMIDAS_FUT, Ficheros.FILE_COMIDAS);
                    ComedorAltamar.comidas.clear();
                    new Ficheros().leerComidas();
                    JOptionPane.showMessageDialog(rootPane,  "Se ha realizado la copia de las comidas de la semana siguiente a la actual.");
                } catch (Exception ex) {
                    Logger.getLogger(JVentana.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
                JOptionPane.showMessageDialog(rootPane, "La actualización de las comidas sólo está permitida los jueves.");
            }
        });
        /*contacto = new JMenuItem(contac);
        contacto.addActionListener((e) -> {
        });*/

        imprimir1.add(imprimirComidas);
        imprimir1.add(imprimirPersonas);
        borrar.add(borrar1);
        borrar.add(borrar2);
        editar.add(borrar);
        editar.addSeparator();
        editar.add(nuevaPersona);
        editar.add(eliminarPersona);
        editar.addSeparator();
        editar.add(eliminarComida);
        editar.addSeparator();
        editar.add(guardar);
        editar.add(actualizar);
        editar.addSeparator();
        editar.add(imprimir1);
        comidass.add(comidasAct);
        comidass.add(comidasFut);

        ver.add(personas);
        ver.addSeparator();
        ver.add(comidass);
        ayuda.add(licencia);
        ayuda.add(instrucciones);
        //ayuda.add(contacto);
        menuBarra.add(editar);
        menuBarra.add(ver);
        menuBarra.add(ayuda);
        add(menuBarra, BorderLayout.NORTH);

        validate();
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cerrar();
            }
        });

    }

    /**
     *
     * @return
     */
    public DefaultTableModel tablaPersonas() {

        String[] columnas = {"Nombre", "Apellidos", "DNI"};

        int i = 0;
        Object[][] data = new Object[ComedorAltamar.alberguistas.size()][3];

        for (String key : ComedorAltamar.alberguistas.keySet()) {
            for (int j = 0; j < 3; j++) {
                switch (j) {
                    case 0:
                        data[i][j] = ComedorAltamar.alberguistas.get(key).getNombre().replace("_", " ");
                        break;
                    case 1:
                        data[i][j] = ComedorAltamar.alberguistas.get(key).getApellidos().replace("_", " ");
                        break;
                    case 2:
                        data[i][j] = ComedorAltamar.alberguistas.get(key).getDNI();
                        break;

                    default:

                }
            }
            i++;
        }

        return new DefaultTableModel(data, columnas);
    }

    /**
     *
     * @return
     */
    public DefaultTableModel tablaComidasActuales() {

        /*NOMBRE  LUNES MARTES MIERCOLES JUEVES VIERNES*/
 /*  MOISES   T|T   N|X*/
        String[] columnas = {"NOMBRE", "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES"};
        Object[][] data = new Object[ComedorAltamar.comidas.size()][6];
        int i = 0;

        for (String key : ComedorAltamar.comidas.keySet()) {
            for (int j = 0; j < 6; j++) {
                switch (j) {
                    case 0: //NOMBRE
                        data[i][j] = ComedorAltamar.alberguistas.get(key).getApellidos().replace("_", " ") + " " + ComedorAltamar.alberguistas.get(key).getNombre().replace("_", " ");
                        break;
                    case 1: //LUNES
                        data[i][j] = ComedorAltamar.comidas.get(key).getComidas().get("L") + "     |     " + ComedorAltamar.comidas.get(key).getCenas().get("L");
                        break;
                    case 2: //MARTES
                        data[i][j] = ComedorAltamar.comidas.get(key).getComidas().get("M") + "     |     " + ComedorAltamar.comidas.get(key).getCenas().get("M");
                        break;
                    case 3: //MIERCOLES
                        data[i][j] = ComedorAltamar.comidas.get(key).getComidas().get("X") + "     |     " + ComedorAltamar.comidas.get(key).getCenas().get("X");
                        break;
                    case 4: //JUEVES
                        data[i][j] = ComedorAltamar.comidas.get(key).getComidas().get("J") + "     |     " + ComedorAltamar.comidas.get(key).getCenas().get("J");
                        break;
                    case 5: //VIERNES
                        data[i][j] = ComedorAltamar.comidas.get(key).getComidas().get("V") + "     |     " + ComedorAltamar.comidas.get(key).getCenas().get("V");
                        break;
                }
            }
            i++;
        }

        return new DefaultTableModel(data, columnas);
    }

    public DefaultTableModel tablaComidasFuturas() {

        /*NOMBRE  LUNES MARTES MIERCOLES JUEVES VIERNES*/
 /*  MOISES   T|T   N|X*/
        String[] columnas = {"NOMBRE", "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES"};
        Object[][] data = new Object[ComedorAltamar.comidas.size()][6];
        int i = 0;

        for (String key : ComedorAltamar.comidas.keySet()) {
            for (int j = 0; j < 6; j++) {
                switch (j) {
                    case 0: //NOMBRE
                        data[i][j] = ComedorAltamar.alberguistas.get(key).getApellidos().replace("_", " ") + " " + ComedorAltamar.alberguistas.get(key).getNombre().replace("_", " ");
                        break;
                    case 1: //LUNES
                        data[i][j] = ComedorAltamar.comidas.get(key).getComidasFuturas().get("L") + "      |      " + ComedorAltamar.comidas.get(key).getCenasFuturas().get("L");
                        break;
                    case 2: //MARTES
                        data[i][j] = ComedorAltamar.comidas.get(key).getComidasFuturas().get("M") + "      |      " + ComedorAltamar.comidas.get(key).getCenasFuturas().get("M");
                        break;
                    case 3: //MIERCOLES
                        data[i][j] = ComedorAltamar.comidas.get(key).getComidasFuturas().get("X") + "      |      " + ComedorAltamar.comidas.get(key).getCenasFuturas().get("X");
                        break;
                    case 4: //JUEVES 
                        data[i][j] = ComedorAltamar.comidas.get(key).getComidasFuturas().get("J") + "      |      " + ComedorAltamar.comidas.get(key).getCenasFuturas().get("J");
                        break;
                    case 5: //VIERNES
                        data[i][j] = ComedorAltamar.comidas.get(key).getComidasFuturas().get("V") + "      |      " + ComedorAltamar.comidas.get(key).getCenasFuturas().get("V");
                        break;
                }
            }
            i++;
        }

        return new DefaultTableModel(data, columnas);
    }

    public void error10() {

        JOptionPane.showMessageDialog(rootPane, error10);
    }

    /*NO HAY COMIDAS*/
    public void error15() {
        JOptionPane.showMessageDialog(rootPane, "Aún no hay residentes anotados para comer/cenar.");

    }

    /*NO EXISTE EL RESIDENTE CON TAL DNI*/
    public void error16() {
        JOptionPane.showMessageDialog(rootPane, "No existe ningún residente con ese DNI.");
    }

    public void cerrar() {
        Object[] opciones = {"Aceptar", "Cancelar"};
        int eleccion = JOptionPane.showOptionDialog(rootPane, "¿Está seguro que desea salir de la aplicación? Asegúrese de haber guardado con antelación.", "Mensaje de Confirmacion",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, opciones, "Aceptar");
        if (eleccion == JOptionPane.YES_OPTION) {
            System.exit(0);
        } else {
        }
    }

    public void eliminarPersona() {

        String inputValue = JOptionPane.showInputDialog("Introduzca el DNI del residente a eliminar.");
        if (ComedorAltamar.alberguistas.containsKey(inputValue)) {
            ComedorAltamar.alberguistas.remove(inputValue);
            ComedorAltamar.comidas.remove(inputValue);
            JOptionPane.showMessageDialog(rootPane, "Residente eliminado");
        } else {
            error16();
        }

    }

    public void añadirPersona() {

        JTextField nombre, apellidos, dni;
        JCheckBox becario;
        JButton aceptar, cancelar;

        JDialog ventana = new JDialog(this, "Nuevo residente");
        ventana.setLayout(new BorderLayout());
        JPanel p1 = new JPanel(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(10, 10, 10, 10);
        constraints.anchor = GridBagConstraints.WEST;

        constraints.gridx = 0;
        constraints.gridy = 0;
        p1.add(new JLabel("Nombre"), constraints);

        nombre = new JTextField(15);
        constraints.gridx = 1;
        constraints.gridy = 0;
        p1.add(nombre, constraints);

        constraints.gridx = 2;
        constraints.gridy = 0;
        p1.add(new JLabel("Apellidos"), constraints);

        apellidos = new JTextField(30);
        constraints.gridx = 3;
        constraints.gridy = 0;
        p1.add(apellidos, constraints);

        constraints.gridx = 4;
        constraints.gridy = 0;
        p1.add(new JLabel("DNI"), constraints);

        dni = new JTextField(9);
        constraints.gridx = 5;
        constraints.gridy = 0;
        p1.add(dni, constraints);

        becario = new JCheckBox("Becario");
        constraints.gridx = 6;
        constraints.gridy = 0;
        p1.add(becario, constraints);

        JPanel p2 = new JPanel(new FlowLayout());
        aceptar = new JButton("Aceptar");
        p2.add(aceptar);
        cancelar = new JButton("Cancelar");
        p2.add(cancelar);

        ventana.add(p1, BorderLayout.CENTER);
        ventana.add(p2, BorderLayout.SOUTH);

        String patron = "(([0-9]{8})([A-Z]))";
        Pattern pattern = Pattern.compile(patron);

        aceptar.addActionListener((e) -> {
            Matcher matcher = pattern.matcher(dni.getText());
            if (nombre.getText().equals("") || apellidos.getText().equals("")) {
                JOptionPane.showMessageDialog(rootPane, "Rellene todos los campos.");
            } else if (ComedorAltamar.alberguistas.containsKey(dni.getText())) {
                JOptionPane.showMessageDialog(rootPane, "Residente ya registrado.");
            } else if (matcher.find()) {
                Persona persona = new Persona(nombre.getText(), apellidos.getText(), dni.getText(), becario.isSelected());
                ComedorAltamar.alberguistas.put(dni.getText(), persona);
                ventana.dispose();
            } else {
                JOptionPane.showMessageDialog(rootPane, "Formato de DNI erroneo.");
            }
        });

        cancelar.addActionListener((e) -> {
            ventana.dispose();
        });

        ventana.pack();
        ventana.setLocationRelativeTo(null);
        ventana.setVisible(true);

    }

    public void imprimir(JTable table) {
        try {
            boolean complete = table.print();
            if (complete) {
                JOptionPane.showMessageDialog(rootPane, "Impresion completada");

            } else {
                JOptionPane.showMessageDialog(rootPane, "La impresión ha sido cancelada.");

            }
        } catch (PrinterException pe) {
            JOptionPane.showMessageDialog(rootPane, "Fallo de impresión");

        }
    }

    public void eliminarComida() {

        String inputValue = JOptionPane.showInputDialog(rootPane, "Introduzca el DNI del residente a eliminar sus comidas.");
        if (ComedorAltamar.alberguistas.containsKey(inputValue)) {
            ComedorAltamar.comidas.remove(inputValue);
            JOptionPane.showMessageDialog(rootPane, "Registro eliminado");
        } else {
            error16();
        }
    }
}
