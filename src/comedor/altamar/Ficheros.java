/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comedor.altamar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author expploitt
 */
public class Ficheros {

    private final String file = "personas.txt";
    static final String FILE_COMIDAS = "comidas.txt";
    static final String FILE_COMIDAS_FUT = "comidasFut.txt";
    private final String cabeceraPersonas = "#####Documento que alberga las personas de la base de datos######\n" + "#Nombre Apellidos DNI Becario#";
    private final String cabeceraComidas = "#####TABLA DE COMIDAS##### \n" + "#IDENTIFICADOR(DNI)-CO:L,N:M,T:X,B:J,N:V,N-CE:...";

    /**
     *
     *
     * @throws java.lang.Exception
     */
    public void leerPersonas() throws Exception {

        String line = "";
        ArrayList<String> lineas = new ArrayList<>();

        try {

            BufferedReader buff = new BufferedReader(new FileReader(file));
            while ((line = buff.readLine()) != null) {
                if (line.contains("#")) {
                    continue;
                }
                lineas.add(line);
            }
            buff.close();

            /*TENEMOS YA LEIDO EL FICHERO, A CONTINUACION GENERAMOS LAS PERSONAS*/
            if (lineas.isEmpty()) {
                throw new Exception("No hay personas");
            }
            /*GENERAMOS LAS DISTINTAS PERSONAS*/
            lineas.forEach((linea) -> {

                String[] partes = linea.split("\\s+");
                Persona alberguista = new Persona(partes[0], partes[1], partes[2], Boolean.getBoolean(partes[3]));
                ComedorAltamar.alberguistas.put(alberguista.getDNI(), alberguista);
            });

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    /**
     *
     */
    public void escribirPersonas() {

        String line = "";
        FileWriter buff;

        try {
            buff = new FileWriter(new File(file));
            buff.write(cabeceraPersonas + "\n");
            for (String key : ComedorAltamar.alberguistas.keySet()) {
                buff.write(ComedorAltamar.alberguistas.get(key).toStringCompleto() + "\n");
            }
            buff.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void leerComidas() throws Exception {

        String line = "";
        HashMap<String, Comida> comidas = new HashMap<>();
        ArrayList<String> lineas = new ArrayList<>();
        BufferedReader buff;

        try {
            buff = new BufferedReader(new FileReader(FILE_COMIDAS));
            while ((line = buff.readLine()) != null) {
                if (line.contains("#")) {
                    continue;
                }
                lineas.add(line);
            }
            /*YA TENEMOS TODAS LAS LINEAS LEIDAS, AHORA GENERAMOS LAS COMIDAS*/

            if (!lineas.isEmpty()) {
                lineas.forEach((string) -> {
                    String[] entrada = string.split("-");
                    new Hilo(0).numeroComidas(entrada);
                });
            } else {
                throw new Exception("No existen comidas");
            }
            buff.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(Ficheros.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void leerComidasFut() throws Exception {

        String line = "";
        HashMap<String, Comida> comidas = new HashMap<>();
        ArrayList<String> lineas = new ArrayList<>();
        BufferedReader buff;

        try {
            buff = new BufferedReader(new FileReader(FILE_COMIDAS_FUT));
            while ((line = buff.readLine()) != null) {
                if (line.contains("#")) {
                    continue;
                }
                lineas.add(line);
            }
            /*YA TENEMOS TODAS LAS LINEAS LEIDAS, AHORA GENERAMOS LAS COMIDAS*/

            if (!lineas.isEmpty()) {
                lineas.forEach((string) -> {
                    String[] entrada = string.split("-");
                    new Hilo(1).numeroComidas(entrada);
                });
            } else {
                throw new Exception("No existen comidas");
            }

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(Ficheros.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void escribirComidas() {

        FileWriter buff, buff2;

        try {

            buff = new FileWriter(new File(FILE_COMIDAS));
            buff2 = new FileWriter(new File(FILE_COMIDAS_FUT));
            buff.write(cabeceraComidas + "\n");
            buff2.write(cabeceraComidas + "\n");

            for (String key : ComedorAltamar.comidas.keySet()) {

                buff.write(ComedorAltamar.comidas.get(key).getIdentificador() + "-");
                buff.write("CO:" + "L," + ComedorAltamar.comidas.get(key).getComidas().get("L") + ":" + "M," + ComedorAltamar.comidas.get(key).getComidas().get("M") + ":"
                        + "X," + ComedorAltamar.comidas.get(key).getComidas().get("X") + ":" + "J," + ComedorAltamar.comidas.get(key).getComidas().get("J") + ":"
                        + "V," + ComedorAltamar.comidas.get(key).getComidas().get("V") + "-");
                buff.write("CE:" + "L," + ComedorAltamar.comidas.get(key).getCenas().get("L") + ":" + "M," + ComedorAltamar.comidas.get(key).getCenas().get("M") + ":"
                        + "X," + ComedorAltamar.comidas.get(key).getCenas().get("X") + ":" + "J," + ComedorAltamar.comidas.get(key).getCenas().get("J") + ":"
                        + "V," + ComedorAltamar.comidas.get(key).getCenas().get("V") + "\n");

                buff2.write(ComedorAltamar.comidas.get(key).getIdentificador() + "-");
                buff2.write("CO:" + "L," + ComedorAltamar.comidas.get(key).getComidasFuturas().get("L") + ":" + "M," + ComedorAltamar.comidas.get(key).getComidasFuturas().get("M") + ":"
                        + "X," + ComedorAltamar.comidas.get(key).getComidasFuturas().get("X") + ":" + "J," + ComedorAltamar.comidas.get(key).getComidasFuturas().get("J") + ":"
                        + "V," + ComedorAltamar.comidas.get(key).getComidasFuturas().get("V") + "-");
                buff2.write("CE:" + "L," + ComedorAltamar.comidas.get(key).getCenasFuturas().get("L") + ":" + "M," + ComedorAltamar.comidas.get(key).getCenasFuturas().get("M") + ":"
                        + "X," + ComedorAltamar.comidas.get(key).getCenasFuturas().get("X") + ":" + "J," + ComedorAltamar.comidas.get(key).getCenasFuturas().get("J") + ":"
                        + "V," + ComedorAltamar.comidas.get(key).getCenasFuturas().get("V") + "\n");
            }

            buff.close();
            buff2.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void borrarPersonas() {

        FileWriter buff;

        try {

            buff = new FileWriter(new File(file));
            buff.write(cabeceraPersonas);
            borrarComidas();
        } catch (IOException ex) {
            Logger.getLogger(Ficheros.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void borrarComidas() {

        FileWriter buff;

        try {

            buff = new FileWriter(new File(FILE_COMIDAS));
            buff.write(cabeceraComidas);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void copiarArchivo(String origen, String destino) {

        BufferedReader in;
        FileWriter out, buff;
        String line = "";

        try {
            in = new BufferedReader(new FileReader(origen));
            out = new FileWriter(new File(destino));

            while ((line = in.readLine()) != null) {
                System.out.println(line);
                out.write(line + "\n");
            }

            in.close();
            out.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Ficheros.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Ficheros.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
