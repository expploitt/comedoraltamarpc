/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comedor.altamar;

import java.util.HashMap;

/**
 *
 * @author expploitt
 */
public class Comida {

    private HashMap<String, String> comidas = new HashMap<>();
    private HashMap<String, String> cenas = new HashMap<>();
    private HashMap<String, String> comidasFuturas = new HashMap<>();
    private HashMap<String, String> cenasFuturas = new HashMap<>();
    private final String identificador;

    public Comida(String identificador) {
        
        this.identificador = identificador;
        this.comidas.put("L", "X");
        this.comidas.put("M", "X");
        this.comidas.put("X", "X");
        this.comidas.put("J", "X");
        this.comidas.put("V", "X");
        
        this.cenas.put("L", "X");
        this.cenas.put("M", "X");
        this.cenas.put("X", "X");
        this.cenas.put("J", "X");
        this.cenas.put("V", "X");
        
        this.comidasFuturas.put("L", "X");
        this.comidasFuturas.put("M", "X");
        this.comidasFuturas.put("X", "X");
        this.comidasFuturas.put("J", "X");
        this.comidasFuturas.put("V", "X");
        
        this.cenasFuturas.put("L", "X");
        this.cenasFuturas.put("M", "X");
        this.cenasFuturas.put("X", "X");
        this.cenasFuturas.put("J", "X");
        this.cenasFuturas.put("V", "X");
    }

    public HashMap<String, String> getCenas() {
        return cenas;
    }

    public HashMap<String, String> getComidas() {
        return comidas;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setComidas(HashMap<String, String> comidas) {
        this.comidas = comidas;
    }

    public void setCenas(HashMap<String, String> cenas) {
        this.cenas = cenas;
    }

     /**
     * @return the comidasFuturas
     */
    public HashMap<String, String> getComidasFuturas() {
        return comidasFuturas;
    }

    /**
     * @param comidasFuturas the comidasFuturas to set
     */
    public void setComidasFuturas(HashMap<String, String> comidasFuturas) {
        this.comidasFuturas = comidasFuturas;
    }

    /**
     * @return the cenasFuturas
     */
    public HashMap<String, String> getCenasFuturas() {
        return cenasFuturas;
    }
     /**
     * @param cenasFuturas the cenasFuturas to set
     */
    public void setCenasFuturas(HashMap<String, String> cenasFuturas) {
        this.cenasFuturas = cenasFuturas;
    }
    
  

}
