/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comedor.altamar;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import javax.swing.JOptionPane;

/**
 *
 * @author expploitt
 */
public class Hilo implements Runnable {

    private final Socket cliente;
    private DataOutputStream out;
    private DataInputStream in;
    private int param;

    public Hilo() {
        this.cliente = null;

    }
    
    public Hilo(int param){
        this.cliente = null;
        this.param = param;
    }

    public Hilo(Socket cliente) throws IOException {
        this.cliente = cliente;
        this.out = new DataOutputStream(cliente.getOutputStream());
        this.in = new DataInputStream(cliente.getInputStream());
    }

    @Override
    public void run() {

        String string;

        try {

            param = in.readInt();
            string = in.readUTF();
            System.out.println(string);
            if (param == 1 || param == 0) {
                trabajaSemana(string);
            } else {
                String[] semana = string.split("\\*");
                param = 0;
                trabajaSemana(semana[0]); //SEMANA ACTUAL  
                param = 1;
                trabajaSemana(semana[1]); //SEMANA FUTURA
            }

            out.close();
            in.close();
            cliente.close();
            /*
            El sistema de tx será:
            IDENTIFICADOR(DNI)-CO:L,N:M,T:X,B:J,N:V,N-CE:...
             */
 /*YA TENEMOS LAS DISTINTAS ANOTACIONES DEL CLIENTE SEPARADAS*/
        } catch (EOFException ex2) {
            System.out.println("Conexión TCP terminada");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        new Ficheros().escribirComidas();

    }

    public void trabajaSemana(String semana) throws IOException {

        String[] entrada;
        String string = "";

        entrada = semana.split("-");

        if (ComedorAltamar.alberguistas.containsKey(entrada[0])) {
            System.out.println("OK");
            this.numeroComidas(entrada);
            out.writeUTF("OK");
            
            switch(param){
                case 0:
                    string = "Semana actual: ";
                    break;
                case 1:
                    string = "Semana siguiente: ";
                    break;
            }
            
            JOptionPane.showMessageDialog(null, "Nueva inscripción en la base de datos " + string + ComedorAltamar.alberguistas.get(entrada[0]).getNombre() + " "
                    + ComedorAltamar.alberguistas.get(entrada[0]).getApellidos().replace("_", " "));
        } else {
            /*Error1*/
            System.out.println("No existe tal dni, ERROR" + entrada[0]);
            out = new DataOutputStream(cliente.getOutputStream());
            out.writeUTF("ERROR1");
        }
    }

    public void numeroComidas(String[] entrada) {

        String[] comida1, comida2;

        comida1 = entrada[1].split(":");
        this.identificarComidas(entrada[0], comida1);
        comida2 = entrada[2].split(":");
        this.identificarComidas(entrada[0], comida2);

    }

    /**
     *
     * @param identificador
     * @param comidas Parametro de la forma: CO L,N M,T X,N J,B...
     * 
     */
    public void identificarComidas(String identificador, String[] comidas) {

        Comida nuevaComida;

        if (param == 0) {
      
            if (ComedorAltamar.alberguistas.containsKey(identificador)) {
                /*NO EXISTE ENTRADA DE COMIDAS Y CENAS DEL ALBERGUISTA*/

                if (!ComedorAltamar.comidas.containsKey(identificador)) {
                    nuevaComida = new Comida(identificador);
                    ComedorAltamar.comidas.put(identificador, nuevaComida);
                }
                /*YA EXISTE UNA ENTRADA DE COMIDAS Y CENAS DEL ALBERGUISTA*/
                if (comidas[0].equals("CO")) {
                    /*YA SABEMOS QUE TENEMOS QUE ANOTAR LAS COMIDAS*/
                    for (int i = 1; i < comidas.length; i++) {
                        String[] partes = comidas[i].split(",");
                        ComedorAltamar.comidas.get(identificador).getComidas().put(partes[0], partes[1]);
                    }
                } else {
                    /*POR DESCARTES YA SABEMOS QUE TENEMOS QUE ANOTAR LAS CENAS*/
                    for (int i = 1; i < comidas.length; i++) {
                        String[] partes = comidas[i].split(",");
                        ComedorAltamar.comidas.get(identificador).getCenas().put(partes[0], partes[1]);
                    }
                }

            }

        } else {/*ES LA SEMANA SIGUIENTE*/
            
            if (ComedorAltamar.alberguistas.containsKey(identificador)) {
                /*NO EXISTE ENTRADA DE COMIDAS Y CENAS DEL ALBERGUISTA*/

                if (!ComedorAltamar.comidas.containsKey(identificador)) {
                    nuevaComida = new Comida(identificador);
                    ComedorAltamar.comidas.put(identificador, nuevaComida);
                }
                /*YA EXISTE UNA ENTRADA DE COMIDAS Y CENAS DEL ALBERGUISTA*/
                if (comidas[0].equals("CO")) {
                    /*YA SABEMOS QUE TENEMOS QUE ANOTAR LAS COMIDAS*/
                    for (int i = 1; i < comidas.length; i++) {
                        String[] partes = comidas[i].split(",");
                        ComedorAltamar.comidas.get(identificador).getComidasFuturas().put(partes[0], partes[1]);
                    }
                } else {
                    /*POR DESCARTES YA SABEMOS QUE TENEMOS QUE ANOTAR LAS CENAS*/
                    for (int i = 1; i < comidas.length; i++) {
                        String[] partes = comidas[i].split(",");
                        ComedorAltamar.comidas.get(identificador).getCenasFuturas().put(partes[0], partes[1]);
                    }
                }

            }
        }
    }
}
