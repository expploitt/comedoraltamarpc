/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comedor.altamar;

/**
 *
 * @author expploitt
 */
public class Persona {
    
    private String nombre;
    private String apellidos;
    private String DNI;
    private boolean becario;
    
    
    public Persona(){
        
    }
    
     public Persona(String nombre, String apellidos, String DNI){
        super();
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.DNI = DNI;
    }
    
    public Persona(String nombre, String apellidos, String DNI, boolean becario){
        super();
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.DNI = DNI;
        this.becario = becario;
    }
    
    public String getNombre(){
        return nombre;
    }
    public String getApellidos(){
        return apellidos;
    }
    public String getDNI(){
        return DNI;
    }
    public boolean getBecario(){
        return becario;
    }

    @Override
    public String toString() {
        return this.apellidos + " " + this.nombre; 
    }
    
    public String toStringCompleto(){
        return this.nombre.replace(" ", "_") + " " + this.apellidos.replace(" ", "_") + " " + this.DNI + " " +this.becario; 
    }
    
}
